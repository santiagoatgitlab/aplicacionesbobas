import React, {useState, useEffect} from 'react'

function Anagrams(props){

    const [ originPhrase, setOriginPhrase ] = useState('')
    const [ editorPhrase, setEditorPhrase ] = useState('')
    const [ errorTimeoutId, setErrorTimeoutId ] = useState(0)
    const [ originLettersMap, setOriginLettersMap ] = useState(new Map())
    const [ lettersToDisplay, setLettersToDisplay ] = useState(new Map())
    const [ lastFailingLetter, setLastFailingLetter ] = useState('')

    function changeOrigin({target}){
        setOriginPhrase(target.value.toLowerCase())
        setEditorPhrase('')
    }

    function changeEditor({target}){
        const text = target.value.toLowerCase()
        if (updateRemainingLetters(text)){
            setEditorPhrase(text)
        }
    }

    function increaseNumberInMap(map, key){
        if (map.has(key)){
            map.set(key, map.get(key) + 1)
        }
        else{
            map.set(key, 1)
        }
    }

    function decreaseNumberInMap(map, key){
        if (map.has(key)){
            if (map.get(key) <= 1){
                map.delete(key)
            }
            else{
                map.set(key, map.get(key) - 1)
            }
        }
    }

    function hideErrorMessage(){
        setErrorTimeoutId(0)
    }

    function updateRemainingLetters(phrase){
        let remainingLettersMap = new Map(originLettersMap)
        for (let editorLetter of phrase){
            if (editorLetter === ' '){
                continue
            }
            if (!remainingLettersMap.has(editorLetter)){
                clearTimeout(errorTimeoutId)
                const timeoutId = setTimeout( hideErrorMessage, 2000 )
                setErrorTimeoutId(timeoutId)
                setLastFailingLetter(editorLetter)
                return false
            }
            decreaseNumberInMap(remainingLettersMap,editorLetter)
        }
        setLettersToDisplay(remainingLettersMap)
        return true
    }

    useEffect( () => {
        let originLettersMap = new Map()
        for (let originLetter of originPhrase){
            if (originLetter !== ' '){
                increaseNumberInMap(originLettersMap, originLetter)
            }
        }
        setOriginLettersMap(originLettersMap)
        setLettersToDisplay(originLettersMap)
    }, [ originPhrase ] ) 

    const remainingLetters = Array.from(lettersToDisplay.entries()).map( ([letter, times]) => {
        let response = ''
        for (let i = 0; i < times; i++){
            response += letter
        }
        return response
    }).filter( letter => letter !== '')

    return (
        <div>
            <button onClick={ () => props.selectApp('')}>Volver</button>
            <div className="app anagrams">
                <h3>Encontrador de anagramas</h3>
                <form>
                    <div>
                        <input
                            type="text"
                            name='originPhrase'
                            placeholder='Ingrese frase de origen'
                            value={originPhrase}
                            onChange={changeOrigin}
                        />
                    </div>
                    <div>
                        <input
                            type="text"
                            name='editorPhrase'
                            placeholder='Vaya escribiendo un anagrama'
                            value={editorPhrase}
                            onChange={changeEditor}
                        />
                    </div>
                </form>
                <div className="remainingLetters">
                    {remainingLetters.join(' ')}
                </div>
                {
                    errorTimeoutId !== 0
                    ?
                        <div className="errorMessage">
                            no hay ninguna letra '{lastFailingLetter}' disponible
                        </div>
                    :
                        null
                }
            </div>
        </div>
    )
}

export default Anagrams
