import React from 'react'

function Menu(props){
    return (
        <ul>
            <li>
                <button  onClick={ () => props.selectApp('anagrams') } value="anagramas">
                    Encontrador de anagramas
                </button>
            </li>
        </ul>
    )
}

export default Menu
