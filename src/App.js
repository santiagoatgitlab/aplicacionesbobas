import React, {useState} from 'react'
import Menu from './Menu'
import Anagrams from './Anagrams'

function App() {

    const [ selectedApp, changeSelectedApp ] = useState('')
    function selectApp(appName){
        changeSelectedApp(appName)
    }
    return (
        <div>
            <h1>Applicaciones bobas</h1>
            <AppSelector appName={selectedApp} selectApp={selectApp} />
        </div>
    )
}

function AppSelector(props){
    switch (props.appName) {
        case 'anagrams' : return <Anagrams selectApp={props.selectApp} />
        default : return <Menu selectApp={props.selectApp} />
    }
}

export default App
